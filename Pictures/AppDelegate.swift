//
//  AppDelegate.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 22/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Titolo largo per UINavigationBar
        UINavigationBar.appearance().prefersLargeTitles = true
        
        //print dei file nella cartella document
        let documentsPath = URL(fileURLWithPath:NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        print("\n")
        AppDelegate.printAllFiles(at : documentsPath, count: 0)
        print("\n")
        
        return true
    }
    
    
    static func printAllFiles(at path : URL, count : Int){
        let items = try! FileManager.default.contentsOfDirectory(atPath: path.path)
        for item in items {
            
            let docpath = path.appendingPathComponent(item)
            var directory : ObjCBool = false
            
            FileManager.default.fileExists(atPath: docpath.path, isDirectory: &directory)
            
            var space = "*\t"
            
            for _ in 0..<count{
                space += "\t"
            }
            print(space + item)
            if directory.boolValue {
                printAllFiles(at: path.appendingPathComponent(item),count: count + 1)
            }
        }
    }
    
}

