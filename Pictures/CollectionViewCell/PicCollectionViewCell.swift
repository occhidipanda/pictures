//
//  PicCollectionViewCell.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 24/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

class PicCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "image_cell"
    @IBOutlet weak var imageViewThumbnail: UIImageView!
    
    func initPlaceholder(){
        self.imageViewThumbnail.contentMode = .center
        self.imageViewThumbnail.image = UIImage(systemName: "photo.on.rectangle")
    }
    
    var image : UIImage? {
        didSet{
            
            guard image != nil else {
                return
            }
            
            self.imageViewThumbnail.contentMode = .scaleAspectFill
            self.imageViewThumbnail.image = image
        }
    }
    
}
