//
//  FavsViewController.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 22/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

class FavsViewController: UIViewController {
    
    @IBOutlet weak var collectionViewFavorites: UICollectionView!
    @IBOutlet weak var labelInfo: UILabel!
    
    private var pictures : [Pictures] = []
    private var picObj : [ChildImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: String(describing: PicCollectionViewCell.self), bundle: nil)
        self.collectionViewFavorites.register(nib, forCellWithReuseIdentifier: PicCollectionViewCell.reuseIdentifier)
        
        self.collectionViewFavorites.delegate = self
        self.collectionViewFavorites.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.pictures = PictureHelper.getFavorites()
        
        // per mantenere la coerenza, il dettaglio delle immagini si aspetta un oggetto di tipo [ChildImage]
        self.picObj = Utils.convert(pictures)
        
        self.labelInfo.isHidden = !pictures.isEmpty
        
        self.collectionViewFavorites.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionViewFavorites.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "image_detail" {
            let destination = segue.destination as? ImageDetailViewController
            destination?.pics = self.picObj
            destination?.currentIndex = sender as? Int
        }
    }
    
}

extension FavsViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PicCollectionViewCell.reuseIdentifier, for: indexPath) as! PicCollectionViewCell
        
        let pic = pictures[indexPath.row]
        
        cell.initPlaceholder()
        
        if let url = URL(string: pic.thumbnail ?? ""), UIApplication.shared.canOpenURL(url) {
            ImageUtils.image(form: url) { (image) in
                UIView.transition(with: cell.imageViewThumbnail, duration: 0.3, options: .transitionCrossDissolve,animations: {
                    if image != nil{
                        cell.image = image
                    }else{
                        cell.initPlaceholder()
                    }
                }, completion: nil)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "image_detail", sender: indexPath.row)
    }
    
}
