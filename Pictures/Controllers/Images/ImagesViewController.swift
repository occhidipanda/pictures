//
//  ImagesViewController.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 22/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit
import Network

enum LayoutStatus {
    case noResults, readyForSearch, displayResults, offline, offlineNoCache
}

class ImagesViewController: UIViewController {
    
    @IBOutlet var toolbar: UIToolbar!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionViewPictures: UICollectionView!
    @IBOutlet weak var labelInfo: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    override var inputAccessoryView: UIView?{
        return toolbar
    }
    
    /*
    
    L'interfaccia viene modulata a seeconda dello stato in cui si trova.
    
    noResults : la ricerca non ha portato a nessun risultato
    readyForSearch : la barra di ricerca è vuota, nessuna ricerca è in corso
    displayResults : il controller sta mostrando i risultati della ricerca
    offlineNoCache : l'app è offline e non ci sono dati salvati in cache
    offline : l'app è offline ma vengono visualizzate le immagini presenti nella cache
    
    */
    var status : LayoutStatus! {
        didSet{
            if oldValue != status {
                switch self.status {
                    case .readyForSearch :
                        
                        self.searchBar.isHidden = false
                        self.collectionViewPictures.isHidden = true
                        self.labelInfo.text = "Scrivi qualcosa nella barra di ricerca 🤓"
                        
                        break
                    case .noResults:
                        
                        self.searchBar.isHidden = false
                        self.collectionViewPictures.isHidden = true
                        self.labelInfo.text = "Nessun risultato per \(self.searchBar.text ?? "") 🧐"
                        
                        break
                    
                    case .displayResults:
                        
                        self.searchBar.isHidden = false
                        self.collectionViewPictures.isHidden = false
                        self.labelInfo.text = ""
                        
                        break
                    
                    case .offline:
                        
                        self.searchBar.isHidden = true
                        self.collectionViewPictures.isHidden = false
                        self.labelInfo.text = ""
                        
                        break
                    
                    case .offlineNoCache:
                        
                        self.searchBar.isHidden = true
                        self.collectionViewPictures.isHidden = true
                        self.labelInfo.text = "Connessione internet assente 🤖"
                        
                        break
                    
                    default: break
                }
            }
        }
    }
    
    var pictures : [ChildImage] = [] {
        didSet{
            self.collectionViewPictures.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Singleton che gestisce i cambi di stato della connessione
        NetworkMonitor.shared.delegate = self
        
        let nib = UINib(nibName: String(describing: PicCollectionViewCell.self), bundle: nil)
        self.collectionViewPictures.register(nib, forCellWithReuseIdentifier: PicCollectionViewCell.reuseIdentifier)
        
        self.collectionViewPictures.delegate = self
        self.collectionViewPictures.dataSource = self
        
        self.searchBar.delegate = self
        
        //Preparazione del layout iniziale a seconda dello stato della connessione
        self.networkDidChangeStatus(status: NetworkMonitor.shared.status)
        
        //Rimozione bordo 
        self.searchBar.backgroundImage = UIImage()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionViewPictures.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - IBActions
    @IBAction func closekeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "image_detail" {
            let destination = segue.destination as? ImageDetailViewController
            destination?.pics = self.pictures
            destination?.currentIndex = sender as? Int
        }
    }
}

extension ImagesViewController : NetworkDelegate {
    
    // Netwotk delgate viene chiamato ogni volta che la connettività cambia di stato
    func networkDidChangeStatus(status: NWPath.Status) {
        
        if status == .satisfied {
            self.searchBar.text = ""
            self.status = .readyForSearch
        }else{
            let picturesCache = PictureHelper.getCachedPictures()
            
            if picturesCache.isEmpty {
                self.status = .offlineNoCache
            }else{
                self.status = .offline
                self.pictures = Utils.convert(picturesCache)
            }
        }
    }
}

extension ImagesViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.labelInfo.text = "Nessun risultato per \(self.searchBar.text ?? "") 🧐"
        
        // il servizio risponde in maniera corretta se viene passata una stringa di 2 o più caratteri
        if searchText.count > 2 {
            PicturesRequest.getImages(of: searchText.lowercased()) { (picture, status) in
                self.pictures = picture?.data.children.filter({$0.data.preview != nil && !$0.data.preview!.images.isEmpty}) ?? []
                
                if self.pictures.isEmpty {
                    self.status = .noResults
                }else{
                    self.status = .displayResults
                }
                
            }
        }else if searchText.isEmpty {
            self.pictures = []
            self.status = .readyForSearch
        }else{
            self.pictures = []
            self.status = .noResults
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
}

extension ImagesViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PicCollectionViewCell.reuseIdentifier, for: indexPath) as! PicCollectionViewCell
        
        let pic = pictures[indexPath.row]
        
        cell.initPlaceholder()
        
        if let url = URL(string: pic.data.thumbnail), UIApplication.shared.canOpenURL(url) {
            
            ImageUtils.image(form: url) { (image) in
                UIView.transition(with: cell.imageViewThumbnail, duration: 0.3, options: .transitionCrossDissolve,animations: {
                    if image != nil{
                        cell.image = image
                    }else{
                        cell.initPlaceholder()
                    }
                }, completion: nil)
            }
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "image_detail", sender: indexPath.row)
    }
    
}
