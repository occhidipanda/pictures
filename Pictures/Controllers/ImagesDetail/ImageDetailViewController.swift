//
//  ImageDetailViewController.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 24/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

class ImageDetailViewController: UIViewController {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var barButtonFavorite: UIBarButtonItem!
    
    var pageViewController : UIPageViewController!
    var pics : [ChildImage]!
    var currentIndex : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelTitle.text = pics[currentIndex].data.title
        
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        
        self.pageViewController.setViewControllers([ImageViewerViewController.getInstance(currentIndex: currentIndex, container: self)], direction: .forward, animated: false, completion: { _ in
            self.barButtonFavorite.image = UIImage(systemName: (self.pics[self.currentIndex].picture?.favorite ?? false) ? "heart.fill" : "heart")
        })
        
    }
    
    
    //MARK: - IBActions
    @IBAction func sharePic(_ sender: Any) {
        
        guard let image = (self.pageViewController.viewControllers?.first as? ImageViewerViewController)?.imageView.image else{
            return
        }
        
        let oggetti = [image] as [Any]
        let activityController = UIActivityViewController(activityItems: oggetti, applicationActivities: [])
        
        if let popOver = activityController.popoverPresentationController {
            popOver.barButtonItem = sender as? UIBarButtonItem
        }
        
        self.present(activityController, animated: true, completion: nil)
        
    }
    
    @IBAction func addToFavs(_ sender: Any) {
        
        guard let index = (pageViewController.viewControllers?.first as? ImageViewerViewController)?.currentIndex, pics[index].picture != nil else{
            return
        }
        
        pics[index].picture?.favorite = !pics[index].picture!.favorite
        barButtonFavorite.image = UIImage(systemName: pics[index].picture!.favorite ? "heart.fill" : "heart")
        
        do{
            try Database.shared.saveContext()
            print("\(pics[index].picture?.id ?? "NO ID") fav: \(pics[index].picture?.favorite ?? false)")
        }catch{
            print(error.localizedDescription)
        }
        
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "page_view_controller"{
            self.pageViewController = segue.destination as? UIPageViewController
        }
    }
    
    
}

extension ImageDetailViewController : UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = (viewController as? ImageViewerViewController)?.currentIndex, index > 0 else {
            return nil
        }
        let prevIndex = index - 1
        return ImageViewerViewController.getInstance(currentIndex: prevIndex, container: self)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = (viewController as? ImageViewerViewController)?.currentIndex, index < pics.count - 1 else {
            return nil
        }
        let nextIndex = index + 1
        return ImageViewerViewController.getInstance(currentIndex: nextIndex, container: self)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let index = (pageViewController.viewControllers?.first as? ImageViewerViewController)?.currentIndex else{
            return
        }
        
        print("current page : \(index)")
        
        self.labelTitle.text = pics[index].data.title
        self.barButtonFavorite.image = UIImage(systemName: (pics[index].picture?.favorite ?? false) ? "heart.fill" : "heart")
        
    }
    
}
