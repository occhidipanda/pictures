//
//  ImageViewerViewController.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 24/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

class ImageViewerViewController: UIViewController {
    
    weak var containerController : ImageDetailViewController?
    
    var childImage : ChildImage? {
        get{
            return containerController?.pics[currentIndex]
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    private(set) var currentIndex : Int!
    private(set) var imageView: UIImageView!
    
    private(set) var image : UIImage! = UIImage(systemName: "photo.on.rectangle")
    private(set) var picture : Pictures?

    static func getInstance(currentIndex : Int, container : ImageDetailViewController) -> ImageViewerViewController {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: ImageViewerViewController.self)) as! ImageViewerViewController
        
        controller.currentIndex = currentIndex
        controller.containerController = container
        
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // L'oggetto Picture contiene le informazioni utili per la cache ed i preferiti
        if let pictureInMemory = childImage!.picture {
            self.picture = pictureInMemory
        }else if let picture = PictureHelper.getPicture(id: self.childImage!.data.id) {
            self.picture = picture
        }else{
            self.picture = PictureHelper.create(id: self.childImage!.data.id, title: self.childImage!.data.title, image: nil, thumbnail: self.childImage!.data.thumbnail)
            try? Database.shared.saveContext()
        }
        
        
        self.containerController!.pics[self.currentIndex].picture = self.picture
        
        self.scrollView.delegate = self
        
        self.imageView = UIImageView()
        self.imageView.tintColor = .systemGray
        self.scrollView.addSubview(self.imageView)
        
        //Per mantenere efficente la visualizzazione delle immagini, una volta scaricata e ridimensionata se troppo grande, viene mantenuto un riferimento in memoria, a tal proposito la visualizzazione delle foto nello slider risulterà più veloce e naturale a discapito di un tempo di attesa iniziale ma che non blocca la fruizione dell'app
        if let imgInMemory = self.containerController!.pics[self.currentIndex].data.preview?.images[0].source.image {
            
            print("Immagine \(String(describing: self.containerController?.pics[self.currentIndex].data.preview?.images[0].source.image)) in memoria")
            
            self.imageView.image = imgInMemory
            self.image = imgInMemory
            self.imageView.sizeToFit()
            
        }else{
            
            self.imageView.image = image
            self.imageView.sizeToFit()
            
            guard let imageUrl = childImage?.data.preview?.images.first?.source.url, let url = URL(string: imageUrl) else {
                self.imageView.image = UIImage(systemName: "bin.xmark")
                return
            }
            
            ImageUtils.image(form: url) { (image) in
            
                guard let i = image else{
                    self.imageView.image = UIImage(systemName: "bin.xmark")
                    return
                }
                
                self.picture?.image = self.childImage!.data.preview?.images.first?.source.url
                
                if max(i.size.height, i.size.width) > 2000 {
                    print("\(self.picture?.id ?? "NO ID") resizing from \(i.size)")
                    DispatchQueue.global(qos: .background).async {
                        guard let newImage = i.resize(scaleFactor: 0.7) else{
                            self.imageView.image = UIImage(systemName: "bin.xmark")
                            return
                        }
                        
                        self.containerController!.pics[self.currentIndex].data.preview?.images[0].source.image = newImage
                        
                        self.picture?.image = imageUrl
                        
                        do{
                            try Database.shared.saveContext()
                        }catch{
                            print(error)
                        }
                        
                        print("\(self.picture?.id ?? "NO ID") resizing to \(newImage.size)")
                        
                        DispatchQueue.main.async {
                            self.refreshImage(img: newImage)
                        }
                    }
                    
                }else{
                    self.refreshImage(img: i)
                }
            }
            
        }
    }
    
    // Per visualizzare correttamente l'immagine del riquadro e calcolare la scala minima dello zoom dobbiamo aspettare che la view principale abbia caricatto tutte le sue subView
    override func viewDidLayoutSubviews() {
        
        self.scrollView.contentSize = image.size
        
        let scrollViewFrame = self.scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / image.size.width
        let scaleHeight = scrollViewFrame.size.height / image.size.height
        let minScale = min(scaleWidth, scaleHeight)
        
        self.scrollView.minimumZoomScale = minScale
        self.scrollView.maximumZoomScale = 1.25
        self.scrollView.zoomScale = minScale
        
    }
    
    func refreshImage(img : UIImage){
        self.image = img
        self.imageView.image = img
        self.imageView.sizeToFit()
        self.viewDidLayoutSubviews()
    }

}

extension ImageViewerViewController : UIScrollViewDelegate {
    
    //ogni volta che procediamo con il pinch to zoom andiamo a centrare la foto nel suo contenitore
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let boundsSize = self.view.bounds.size
        var contentsFrame = self.imageView.frame
        
        if contentsFrame.size.width < boundsSize.width {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0
        } else {
            contentsFrame.origin.x = 0.0
        }
        
        if contentsFrame.size.height < boundsSize.height {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0
        } else {
            contentsFrame.origin.y = 0.0
        }
        
        imageView.frame = contentsFrame
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
