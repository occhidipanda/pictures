//
//  DatabaseHelper.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 24/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit
import CoreData

//Per facilitare la gestione delle immagini salvate nel filesystem e i preferiti è stato creato un sqlite con CoreData. 
class Database {
    
    private let coreDataFileName = "Pictures"
    
    static let shared = Database()
    private(set) var managedObjectContext : NSManagedObjectContext!
    private var persistentStoreCoordinator : NSPersistentStoreCoordinator!
    private var managedObjectModel : NSManagedObjectModel!
    
    private init(){
        
        guard let docFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
            fatalError("La url della cartella documents non esiste!!!")
        }
        
        let modelURL = Bundle.main.url(forResource: coreDataFileName, withExtension: "momd")!
        managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        let url = docFolder.appendingPathComponent("\(coreDataFileName).sqlite")
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [:])
            managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
            
            print("Database inizializzato")
        } catch let  error as NSError {
            print("Errore inizializzazione DB \(error.description)")
        }
    }
    
    func saveContext() throws {
        if managedObjectContext?.hasChanges ?? false {
            try managedObjectContext.save()
            print("Dati salvati")
        }
    }
    
}
