//
//  PictureHelper.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 24/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit
import CoreData

// Classe che semplifica le operazione di lettura dal database
class PictureHelper: NSObject {
    
    static let entityName = "Pictures"
    
    // Metodo che restituisce tutti gli oggetti che hanno in riferimento un'immagine nel filesystem, se l'immagine non è stata correttamente scaricata il campo "image" risulterà nil
    static func getCachedPictures() -> [Pictures]{
        let request = NSFetchRequest<Pictures>(entityName : entityName)
        request.predicate = NSPredicate(format: "image != nil")
        do{
            return try Database.shared.managedObjectContext.fetch(request)
        }catch{
            print(#function)
            print(error.localizedDescription)
            return []
        }
    }
    
    
    
    //Metodo che restituisce tutti i riferimenti alle immagini preferite
    static func getFavorites() -> [Pictures] {
        let request = NSFetchRequest<Pictures>(entityName : entityName)
        request.predicate = NSPredicate(format: "image != nil && favorite == true")
        do{
            return try Database.shared.managedObjectContext.fetch(request)
        }catch{
            print(#function)
            print(error.localizedDescription)
            return []
        }
    }
    
    // Metodo che restituisce una determinata immagine a secoda dell'id passato ottenuto dal servizio
    static func getPicture(id : String) -> Pictures? {
        let request = NSFetchRequest<Pictures>(entityName : entityName)
        request.predicate = NSPredicate(format: "id == %@", id)
        do{
            return try Database.shared.managedObjectContext.fetch(request).first
        }catch{
            print(#function)
            print(error.localizedDescription)
            return nil
        }
    }
    
    // Creazione di un'oggetto Pictures che viene inserito nel ManagedObjectModel
    static func create(id : String, title : String, image: String?, thumbnail : String?) -> Pictures {
        let picture = NSEntityDescription.insertNewObject(forEntityName: entityName, into: Database.shared.managedObjectContext) as! Pictures
        
        picture.id = id
        picture.image = image
        picture.title = title
        picture.thumbnail = thumbnail
        picture.favorite = false
        
        return picture
    }

}
