//
//  PicturesRequest.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 23/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

class PicturesRequest: NSObject {
    
    private static let baseUrl = "https://www.reddit.com/r/%@/top.json"
    
    static func getImages(of topic : String, completion : @escaping (PicturesResponse?, Int) -> Void) {
    
        let urlString = String(format: baseUrl, topic)
        let queryItem = URLQueryItem(name: "raw_json", value: "1") // parametro per disabilitare la url-encoding delle stringhe 
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        Networking.get(url: url, params: [queryItem], headers: [:], completion: completion)
    }

}
