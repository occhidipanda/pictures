//
//  Picture.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 23/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

// La struttura viene creata con l'interfaccia Decodable. In questo modo siamo in grado di decodificare un JSON in maniera estremamente veloce e flessibile

struct PicturesResponse : Decodable {
    var data : ChildrenContainer
}
struct ChildrenContainer : Decodable {
    var children : [ChildImage]
}
struct ChildImage : Decodable {

    var data : ImageInfo
    var picture : Pictures?
    
    private enum CodingKeys : String, CodingKey {
        case data
    }
}
struct ImageInfo : Decodable {
    let id : String
    let thumbnail : String
    let title : String
    var preview : Preview?
}
struct Preview : Decodable {
    var images : [Image]
}
struct Image : Decodable {
    let id : String
    var source : Source
   
}
struct Source : Decodable {
    let url : String
    var image : UIImage?
    
    private enum CodingKeys : String, CodingKey {
        case url
    }
}


