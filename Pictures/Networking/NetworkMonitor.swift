//
//  NetworkMonitor.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 23/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit
import Network

protocol NetworkDelegate {
    func networkDidChangeStatus(status : NWPath.Status)
}


// classe che sfrutta il framework Network per intercettare i cambi di stato della connessione
class NetworkMonitor: NSObject {
    
    static let shared = NetworkMonitor()
    var delegate : NetworkDelegate?
    
    private(set) var status : NWPath.Status = .unsatisfied

    private override init() {
        super.init()
        
        let networkMonitor = NWPathMonitor()
        
        networkMonitor.pathUpdateHandler = { path in
            self.status = path.status
            
            print("\(NetworkMonitor.self) - status changed : \(path.status)")
            
            DispatchQueue.main.async {
                self.delegate?.networkDidChangeStatus(status: path.status)
            }
        }
        
        networkMonitor.start(queue: DispatchQueue(label: "NetworkMonitor"))
    }
    

}
