//
//  Networking.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 22/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit
import Network

enum ErrorCodes : Int {
    case noConnection = -1
    case unknowError = -2
    case jsonError = -3
}

// Classe generica per effettuare chiamate a servizi rest, limitata all'utilizzo del metodo get
class Networking: NSObject {
    
    private static func execute<T : Decodable>(_ url : URL, method : String, headers : [String:String], callback : @escaping (T?, Int) -> Void){
        
        print("url: \(url.absoluteString)")
        print("method \(method)")
        print("headers \(headers)")
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            
            print((response as? HTTPURLResponse)?.statusCode ?? "HTTPURLResponse == nil")
            
            if let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode == 200 {
                do{
                    
                    print("STATUS 200")
                    print("response: \(String(data: data ?? Data(), encoding: .utf8) ?? "EMPTY")")
                    let object = try JSONDecoder().decode(T.self, from: data ?? Data())
                    DispatchQueue.main.async {
                        callback(object, statusCode)
                    }
                }catch{
                    print("ERROR: \(error.localizedDescription)")
                    DispatchQueue.main.async {
                        callback(nil, ErrorCodes.jsonError.rawValue)
                    }
                }
                
            }else {
                DispatchQueue.main.async {
                    callback(nil, (response as? HTTPURLResponse)?.statusCode ?? ErrorCodes.unknowError.rawValue)
                }
                return
            }
        }).resume()
        
    }
    
    static func get<T : Decodable>(url : URL, params : [URLQueryItem], headers : [String:String], completion : @escaping (T?, Int) -> Void){
        
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponents?.queryItems = params
        
        guard let url = urlComponents?.url else{
            completion(nil, ErrorCodes.unknowError.rawValue)
            return
        }
        
        Networking.execute(url, method: "get", headers: headers, callback: completion)
        
    }
}
