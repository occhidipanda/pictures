//
//  ImageUtils.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 22/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

extension UIImage {
    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
        self.init(data: imageData)
    }
    
    func resize(scaleFactor : CGFloat) -> UIImage? {

        let imageSize = CGSize(width: size.width * scaleFactor, height: size.height * scaleFactor)
        UIGraphicsBeginImageContext(imageSize)
        draw(in: CGRect(origin: .zero, size: imageSize))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resizedImage
    }
    
}

// Download e salvataggio in locale delle immagini
class ImageUtils: NSObject {
    
    //Metodo che permette di caricare una immagine dal filesystem se presente oppure di scaricarla dalla url, il nome dell'immagine deve corrispondere all'ultimo componente della url
    static func image(form url : URL, completion: @escaping (UIImage?) -> Void){
        ImageUtils.image(fromCache: url.lastPathComponent) { (cacheImage) in
            if let i = cacheImage {
                print("Imagine da filesystem \(url.lastPathComponent)")
                completion(i)
            }else{
                print("download in corso \(url.absoluteURL)")
                image(downloadFrom: url, cache: true) { (image, error) in
                    print("immagine = \(image != nil) errori: \(error?.localizedDescription ?? "NO ERROR")")
                    completion(image)
                }
            }
        }
    }
    
    //Metodo che carica un'immagine dalla cartella document dell'app
    static func image(fromCache title : String, completion: @escaping (UIImage?) -> Void){
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let dirPath = paths.first {
            DispatchQueue.global(qos: .background).async {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(title.replacingOccurrences(of: "/", with: "-"))
                let image = UIImage(contentsOfFile: imageURL.path)
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }else{
            DispatchQueue.main.async {
                completion(nil)
            }
        }
    }
    
    // Metodo che scarica l'immagine da una url ed eventualmente la salva sul filesystem attribuendo come nome l'ultimo componente della url data
    static func image(downloadFrom url: URL, cache : Bool, completion: @escaping (UIImage?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let e = error {
                DispatchQueue.main.async {
                    completion(nil,e)
                }
            }else{
                
                guard let d = data, let image = UIImage(data: d) else{
                    DispatchQueue.main.async {
                        completion(nil,nil)
                    }
                    return
                }
                
                if cache {
                    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                    let fileName = url.lastPathComponent
                    let fileURL = documentsDirectory.appendingPathComponent(fileName)
                    if let data = image.jpegData(compressionQuality: 1), !FileManager.default.fileExists(atPath: fileURL.path) {
                        do {
                            try data.write(to: fileURL)
                            debugPrint("IMAGE \(fileURL.absoluteString) SAVED")
                            DispatchQueue.main.async {
                                completion(image,nil)
                            }
                        } catch {
                            debugPrint("ERROR SAVING IMAGE: ", error)
                            DispatchQueue.main.async {
                                completion(image,error)
                            }
                        }
                    }else{
                        completion(image, nil)
                    }
                }else{
                    DispatchQueue.main.async {
                        completion(image,nil)
                    }
                }
            }
        }.resume()
    }

}
