//
//  Utils.swift
//  Pictures
//
//  Created by Tiziano Caccavo on 24/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    //Per questione di coerenza in alcuni casi è necessario convertire un oggetto [Pictures] in [ChildImage]
    static func convert(_ pics : [Pictures]) -> [ChildImage] {
        var picObj : [ChildImage] = []
        for pic in pics {
            
            let source = Source(url: pic.image ?? "", image: nil)
            let images = [Image(id: pic.id ?? "", source: source)]
            let preview = Preview(images: images)
            let imageInfo = ImageInfo(id: pic.id ?? "", thumbnail: pic.thumbnail ?? "", title: pic.title ?? "", preview: preview)
            picObj.append(ChildImage(data: imageInfo, picture: pic))
            
        }
        return picObj
    }

}
