//
//  PicturesTests.swift
//  PicturesTests
//
//  Created by Tiziano Caccavo on 22/04/2020.
//  Copyright © 2020 Tiziano Caccavo. All rights reserved.
//

import XCTest
@testable import Pictures

class PicturesTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    func testGetImagesUrl(){
        
        let expectation = XCTestExpectation(description: "Getting image urls from Reddit")
        
        PicturesRequest.getImages(of: "cat") { (picture, status) in
            XCTAssert(status == 200 && picture != nil)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testSaveImagesInCache(){
        
        let url = URL(string: "https://b.thumbs.redditmedia.com/SZkHoWBMYNAdOW2Q1JsIflgl0-nLL2TQnG2gltYHCuM.jpg")
        XCTAssertNotNil(url)
        
        let expectation = XCTestExpectation(description: "Saving Image")
        
        ImageUtils.image(downloadFrom: url!, cache: true) { (image, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(image)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testGetImageFromCache(){
        
        let url = URL(string: "https://b.thumbs.redditmedia.com/SZkHoWBMYNAdOW2Q1JsIflgl0-nLL2TQnG2gltYHCuM.jpg")
        XCTAssertNotNil(url)
        
        let expectation = XCTestExpectation(description: "Getting Image")
        
        ImageUtils.image(downloadFrom: url!, cache: true) { (image, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(image)
            ImageUtils.image(fromCache: url!.lastPathComponent) { (image) in
                XCTAssertNotNil(image)
                expectation.fulfill()
            }
            
        }
        
        wait(for: [expectation], timeout: 10)
        
    }

}
